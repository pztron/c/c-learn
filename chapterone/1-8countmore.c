#include <stdio.h>   
/* count lines, blanks, and tabs in input */   
int main() {       
    int c, nsc;       
    nsc = 0;       
    while ((c = getchar()) != EOF)           
        if (c == '\n' || '\t' || ' ')               
            ++nsc;
            printf("%d %s\n", c, "It Happened");      
    printf("%d\n", nsc);   
}